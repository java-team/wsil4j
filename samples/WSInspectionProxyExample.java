/*
 * The Apache Software License, Version 1.1
 *
 *
 * Copyright (c) 1999 The Apache Software Foundation.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Apache Software Foundation (http://www.apache.org/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "xml-axis-wsil" and "Apache Software Foundation" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact apache@apache.org.
 *
 * 5. Products derived from this software may not be called "Apache",
 *    nor may "Apache" appear in their name, without prior written
 *    permission of the Apache Software Foundation.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 */

import org.apache.wsil.*;
import org.apache.wsil.client.*;
import org.apache.wsil.util.*;

import java.util.*;


/**
 * This is an example of how to use the WSILProxy class
 * to read the contents of a WS-Inpsection document.
 *
 * @version: 1.0
 * @author: Peter Brittenham
 */
public class WSInspectionProxyExample 
{
  /**
   * WSInspectionProxyExample.
   */
  public WSInspectionProxyExample() 
  {
  }
  
  
  /**
   * Use the WS-Inspection proxy to read a WS-Inspection document.
   *
   * @param args an array of command-line arguments
   */
  public static void main(String[] args) 
  {
    String wsinspectionURL;
    String serviceName;
	  
    try
    {
      // If there is no input argument, then throw exception
      if (args.length < 2)
      {
        throw new IllegalArgumentException("Usage: WSInspectionProxyExample documentURL serviceName");
      }

      // Get the location of the WS-Inspection document
      wsinspectionURL = args[0];
      serviceName = args[1];

      // Create a new instance of a WS-Inspection document proxy
      WSILProxy proxy = new WSILProxy(wsinspectionURL);

      // Get all of the WSDL documents using the input service name
      WSDLDocument[] wsdlDocuments = proxy.getWSDLDocumentByServiceName(serviceName);

      // Display purpose of list
      System.out.println("Display contents of WSDL service description documents for service name [" +
                         serviceName + "]...");
		  
      // Process each WSDL document reference
      for (int wsdlCount = 0; wsdlCount < wsdlDocuments.length; wsdlCount++)
      {
        // Display contents of the document
        System.out.println("[" + wsdlCount + "] \n" + wsdlDocuments[wsdlCount].serializeToXML());
      }
    }

    catch (Exception e)
    {
      // Display exception message
      System.out.println("EXCEPTION: " + e.getMessage());
      e.printStackTrace();
    }

    // Exit
    System.exit(0);
  }
}
