/*
 * The Apache Software License, Version 1.1
 *
 *
 * Copyright (c) 1999 The Apache Software Foundation.  All rights 
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product includes software developed by the
 *        Apache Software Foundation (http://www.apache.org/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 4. The names "xml-axis-wsil" and "Apache Software Foundation" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written 
 *    permission, please contact apache@apache.org.
 *
 * 5. Products derived from this software may not be called "Apache",
 *    nor may "Apache" appear in their name, without prior written
 *    permission of the Apache Software Foundation.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 */

import org.apache.wsil.*;
import org.apache.wsil.extension.wsdl.*;

import java.util.*;


/**
 * An example of how to read and use the contents of
 * a WS-Inpsection document.
 *
 * @version: 1.0
 * @author: Peter Brittenham
 */
public class WSInspectionExample 
{
  /**
   * WSInspectionExample.
   */
  public WSInspectionExample() 
  {
  }
  
  
  /**
   * Read the specified WS-Inspection document and list all 
   * of the WSDL documents.
   * 
   * @param args an array of command-line arguments
   */
  public static void main(String[] args) 
  {
    String wsinspectionURL;
    Description[] descriptions;
    String serviceName;
    Vector wsdlList = new Vector();
	  
    try
    {
      //If there is no input argument, then throw exception
      if (args.length == 0)
      {
        throw new IllegalArgumentException("Usage: WSInspectionExample documentURL");
      }

      // Get the location of the WS-Inspection document
      wsinspectionURL = args[0];

      // Create a new instance of a WS-Inspection document
      WSILDocument document = WSILDocument.newInstance();

      // Read and parse the WS-Inspection document
      document.read(args[0]);

      // Get the inspection element from the document
      Inspection inspection = document.getInspection();

      // Obtain a list of all service elements
      Service[] services = inspection.getServices();

      // Display purpose of list
      System.out.println("Display list of WSDL service description references...");
		  
      // Process each service element to find all WSDL document references
      for (int serviceCount = 0; serviceCount < services.length; serviceCount++)
      {
        // Get the next set of description elements
        descriptions = services[serviceCount].getDescriptions();

        // Process each description to find the WSDL references
        for (int descCount = 0; descCount < descriptions.length; descCount++)
        {
          // If the referenced namespace is for WSDL, then save the location reference
          if (descriptions[descCount].getReferencedNamespace().equals(WSDLConstants.NS_URI_WSDL))
          {
            // Add WSDL location to the list
            wsdlList.add(descriptions[descCount].getLocation());
          }
        }

        // If this service has WSDL service descriptions, then display the list
        if (wsdlList.size() > 0)
        {
          // Get service name
          serviceName = (services[serviceCount].getServiceNames().length == 0) ? 
                        "[no service name]" : services[serviceCount].getServiceNames()[0].getText();
				                
          // Display service name
          System.out.println("  Service: " + serviceName);

          // Display list
          Iterator iterator = wsdlList.iterator();
          for (int count = 1; iterator.hasNext(); count++)
          {
            System.out.println("    [" + count + "] " + ((String) iterator.next()));
          }
        }

        // Clear the list
        wsdlList.clear();
      }
    }

    catch (Exception e)
    {
      // Display exception message
      System.out.println("EXCEPTION: " + e.getMessage());
      e.printStackTrace();
    }

    // Exit
    System.exit(0);
  }
}
