<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Web Services Inspection Language (WS-Inspection) Overview</title>
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="keywords" content="specific keywords for this article"/>
<META name="GENERATOR" content="IBM WebSphere Studio Homepage Builder V6.0.0 for Windows">
<META http-equiv="Content-Style-Type" content="text/css">
<LINK rel="stylesheet" href="wsil4j.css" type="text/css">
</head>
<BODY>
<H1>Web Services Inspection Language
<BR>(WS-Inspection)</H1>
<HR>
<H2>Dis<span class="atitle2">tributed Web Service Discovery Using WS-Inspection Documents</span></H2>
<blockquote>Service discovery defines a process for locating service providers and retrieving 
service description documents, and is a key component of the overall Web services model.  
Service discovery is a very broad concept, which means that it is unlikely to have one solution 
that addresses all of its requirements.  The Universal Description, Discovery and Integration 
(UDDI) specification addresses a subset of the overall requirements by using a centralized service 
discovery model.  This article will provide an overview of the Web Services Inspection Language 
(WS-Inspection), which is another related service discovery mechanism, but addresses a different 
subset of requirements using a distributed usage model.  The WS-Inspection is designed around an 
XML-based model for building an aggregation of references to existing Web service descriptions, 
which are exposed using standard Web server technology.</blockquote>
<P>The Web Services architecture is based upon the interactions between three
primary roles: service provider, service registry, and service requestor.
These roles interact using publish, find and bind operations. The service
provider is the business that provides access to the Web service and publishes
the service description in a service registry. The service requestor finds
the service description in a service registry and uses the information
in the description to bind to a service. A logical view of the Web Services
architecture is shown in Figure 1. In this view of the Web Services architecture,
the service registry provides a centralized location for storing service
descriptions. A UDDI registry is an example of this type of service registry.</P>
<p><a name="figure1"></a><b>Figure 1: Web Services Architecture</b>

<p><IMG src="images/WebServicesArchitecture.jpg" width="430" height="340" border="2"><P>Although it is important, the centralized service registry is not the only
model for Web service discovery. The simplest form of service discovery
is to request a copy of the service description from the service provider.
After receiving the request, the service provider can simply email the
service description as an attachment or provide it to the service requestor
on a transferable media, such as a diskette. Although this type of service
discovery is simple, it is not a very efficient since it requires prior
knowledge of the Web service, as well as the contact information for the
service provider.</P>
<P>Between these two extremes, there is a need for a distributed service discovery
method that provides references to service descriptions at the service
provider's point-of-offering. The Web Services Inspection Language provides
this type of distributed discovery method, by specifying how to inspect
a web site for available Web services. The WS-Inspection specification
defines the locations on a Web site where you could look for Web service
descriptions.</P>
<P>Since the Web Services Inspection Language focuses on distributed service
discovery, the WS-Inspection specification complements UDDI by facilitating
the discovery of services available on Web sites, but which may not yet
be listed in a UDDI registry. Additional information on the relationship
between the Web Services Inspection Language and UDDI can be found in <I>The WS-Inspection and UDDI Relationship</I> (see <A href="#resources">Resources</A>).</P>
<H2>WS-Inspection Overview</H2>
<P>The WS-Inspection specification (see <A href="#resources">Resources</A>) does not define a service description language. WS-Inspection documents
provide a method for aggregating different types of service descriptions.
Within a WS-Inspection document, a single service can have more than one
reference to a service description. For example, a single Web service might
be described with both a WSDL file and within UDDI. References to these
two service descriptions should be put into a WS-Inspection document. If
multiple references are available, it is beneficial to put all of them
in the WS-Inspection document so that the document consumer can select
the type of service description that they are capable of understanding
and want to use. Figure 2 provides an overview of how WS-Inspection documents
are used.</P>
<p><a name="figure2"></a><b>Figure 2: WS-Inspection Overview</b>
<P><IMG src="images/WSInspectionOverview.jpg" width="560" height="480" border="2"></P>
<p>The WS-Inspection specification contains two primary functions, which are
discussed in more detail in the next two sections.</p>
<UL>
  <LI>It defines an XML format for listing references to existing service descriptions.
  <LI>It defines a set of conventions so that it is easy to locate WS-Inspection
  documents. 
</UL>
<H2>WS-Inspection Document Format</H2>
<P>A WS-Inspection document provides an aggregation of references to service
descriptions. These service descriptions can be defined in any service
description format, such as WSDL, UDDI, or plain HTML. As mentioned previously,
a WS-Inspection document is generally made available at the point-of-offering
for the services that are referenced within the document.
<p>A WS-Inspection document can contain a list of references to service descriptions, as well as references to other WS-Inspection documents. A WS-Inspection document will contain one or more <CODE>&lt;service&gt;</CODE> and <CODE>&lt;link&gt;</CODE> elements. A <CODE>&lt;service&gt;</CODE> element will contain one or more references to different types of service descriptions for the same Web service. The <CODE>&lt;link&gt;</CODE> element may contain references to only one type of service description,
but these service descriptions do not have to reference the same Web service.</p>
<p>Listing 1 contains a simple example of a WS-Inspection document. This example
contains two references to different service descriptions, and a single
reference to another WS-Inspection document. The first <CODE>&lt;service&gt;</CODE> element contains only one service description, and it is a reference to a WSDL document. The second <CODE>&lt;service&gt;</CODE> element also contains only one service description reference. This reference is to a business service entry in a UDDI registry. The UDDI service key identifies one unique business service. The UDDI service reference also contains extensibility elements which are discussed in the next section. The <SAMP>&lt;link&gt;</SAMP> element is used to reference a collection of service descriptions. In this case, it is referencing another WS-Inspection document.</p>
<p><a name="listing1"></a><b>Listing 1: Example WS-Inspection Document</b>

<table border="1" cellpadding="5"  width="100%" cellspacing="0" bgcolor="#cccccc"><tr><td><pre><code>&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;inspection xmlns=&quot;http://schemas.xmlsoap.org/ws/2001/10/inspection/&quot;&gt;
  &lt;service&gt;
    &lt;description referencedNamespace=&quot;http://schemas.xmlsoap.org/wsdl/&quot;
                 location=&quot;http://example.com/exampleservice.wsdl&quot; /&gt;
  &lt;/service&gt;
  &lt;service&gt;
    &lt;description referencedNamespace=&quot;urn:uddi-org:api&quot;&gt;
       &lt;wsiluddi:serviceDescription location=&quot;http://example.com/uddi/inquiryapi&quot;&gt;
         &lt;wsiluddi:serviceKey&gt;52946BB0-BC28-11D5-A432-0004AC49CC1E&lt;/wsiluddi:serviceKey&gt;
       &lt;/wsiluddi:serviceDescription&gt;
    &lt;/description&gt;
  &lt;/service&gt;
  &lt;link referencedNamespace=&quot;http://schemas.xmlsoap.org/ws/2001/10/inspection/&quot;
        location=&quot;http://example.com/tools/toolservices.wsil&quot;/&gt;
&lt;/inspection&gt;</code></pre></td></tr></table>
<H3><b>WS-Inspection document extensibility</b></H3>
<P>The WS-Inspection specification does not limit the type of service descriptions that can be referenced. Both the <CODE>&lt;description&gt;</CODE> and <CODE>&lt;link&gt;</CODE> element may contain extensibility elements that represent information
for a specific service description technology. The WS-Inspection specification
defines a set of standard extensibility elements for both WSDL and UDDI.
Since the <CODE>&lt;description&gt;</CODE> element is used to reference a single service description and the <CODE>&lt;link&gt;</CODE> element is used to reference one or more sets of service descriptions,
any extensibility elements that are defined for these elements should follow
this same pattern.</P>
<P>WSDL service descriptions can only be referenced from within a <CODE>&lt;description&gt;</CODE> element. The WSDL extensibility elements can be used to indicate whether
or not the WSDL document contains an endpoint specification. If there is
more than one service element in the WSDL document, then the <CODE>&lt;wsilwsdl:referencedService&gt;</CODE> element should be used to indicate which one is associated with the entry in the WS-Inspection document. One or more <CODE>&lt;wsilwsdl:implementedBinding&gt;</CODE> elements may appear in WSDL service description reference. Each of these
elements references a binding that is implemented by the WSDL document.
Listing 2 contains an example of a WS-Inspection document that contains
all of the WSDL extensibility elements.</P>
<p><a name="listing1"></a><b>Listing 2: WS-Inspection Document with WSDL Extensibility Elements</b>
<table border="1" cellpadding="5"  width="100%" cellspacing="0" bgcolor="#cccccc">
<tr><td>
      <PRE><code>&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;inspection xmlns=&quot;http://schemas.xmlsoap.org/ws/2001/10/inspection/&quot;&gt;
  ...
  &lt;service&gt;
    &lt;name xml:lang=&quot;en-US&quot;&gt;StockQuoteService&lt;/name&gt;
    &lt;description referencedNamespace=&quot;http://schemas.xmlsoap.org/wsdl/&quot; 
      &lt;wsilwsdl:reference endpointPresent=&quot;true&quot;&gt;
                 location=&quot;http://localhost:8080/webservices/wsdl/stockquote/sqs.wsdl&quot;&gt;
        &lt;wsilwsdl:referencedService 
            xmlns:tns=&quot;http://www.getquote.com/StockQuoteService&quot;&gt;
          tns:StockQuoteService
        &lt;/wsilwsdl:referencedService&gt;
        &lt;wsilwsdl:implementedBinding 
            xmlns:interface=&quot;http://www.getquote.com/StockQuoteService-interface&quot;&gt;
          interface:StockQuoteServiceBinding
        &lt;/wsilwsdl:implementedBinding&gt;
      &lt;/wsilwsdl:reference&gt;
    &lt;/description&gt;
  &lt;/service&gt;
  ...
&lt;/inspection&gt;</code></PRE>
      </td></tr></table>
<P>The UDDI extensibility elements may appear within either the <CODE>&lt;link&gt;</CODE> or <CODE>&lt;description&gt;</CODE> elements. The elements used within the <CODE>&lt;link&gt;</CODE> element can only reference a UDDI business entity. Since this element
references a UDDI business entity, resolving this reference will result
in one or more service descriptions. The elements used within the <CODE>&lt;description&gt;</CODE> element may only reference a single UDDI business service. Listing 3 contains
an example of the UDDI bindings for a WS-Inspection document.</P>
<P>The <CODE>&lt;wsiluddi:businessDescription&gt;</CODE> element is used within a <CODE>&lt;link&gt;</CODE> element to specify a reference to a UDDI business entity. The businessService
element may contain either a discoveryURL, or a businessKey, or both. If
a businessKey is specified, then the location attribute on the businessDescription
element must contain an inquiry URL for a UDDI registry. This URL is used
to send a <I>get_businessDetail</I> messsage to the UDDI registry using the businessKey that was specified.</P>
<P>The <CODE>&lt;wsiluddi:serviceDescription&gt;</CODE> element can only be used within the <CODE>&lt;description&gt;</CODE> element, and can reference only one service description. Within the serviceDescription
element, a discoveryURL, a serviceKey, or both can be specified. The location
attribute on the serviceDescription must contain the inquiry URL for a
UDDI registry, when the serviceKey is specified.</P>
<P>For both the businessDescription and serviceDescription elements, if both
the discoveryURL and the businessKey or serviceKey are specified, then
the person who is processing the WS-Inspection document can select which
one they want to use. The discoveryURL will always return a UDDI business
entity. So when it is used with the serviceDescription element, the serviceKey
must be used to locate the individual service description within the business
entity.</P>
<p><a name="listing1"></a><b>Listing 3: WS-Inspection Document with UDDI Extensibility Elements</b>


<table border="1" cellpadding="5"  width="100%" cellspacing="0" bgcolor="#cccccc">

    <tr>
      <td>
      <PRE><code>&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;inspection targetNamespace=&quot;http://schemas.xmlsoap.org/ws/2001/10/inspection/&quot;
    xmlns:wsiluddi=&quot;http://schemas.xmlsoap.org/ws/2001/10/inspection/uddi/&quot;
    xmlns=&quot;http://schemas.xmlsoap.org/ws/2001/10/inspection/&quot;&gt;
  &lt;link referencedNamespace=&quot;urn:uddi-org:api&quot;&gt;
    &lt;wsiluddi:businessDescription location=&quot;http://www.getquote.com/uddi/inquiryapi&quot;&gt;
      &lt;wsiluddi:businessKey&gt;3BF0ACC0-BC28-11D5-A432-0004AC49CC1E&lt;/wsiluddi:businessKey&gt;
      &lt;wsiluddi:discoveryURL useType=&quot;businessEntity&quot;&gt;
        http://www.getquote.com/uddi?businessKey=3BF0ACC0-BC28-11D5-A432-0004AC49CC1E
      &lt;/wsiluddi:discoveryURL&gt;
    &lt;/wsiluddi:businessDescription&gt;
  &lt;/link&gt;
  &lt;service&gt;
    &lt;name&gt;UDDI Service Description&lt;/name&gt;
    &lt;description referencedNamespace=&quot;urn:uddi-org:api&quot;&gt;
      &lt;wsiluddi:serviceDescription location=&quot;http://www.getquote.com/uddi/inquiryapi&quot;&gt;
        &lt;wsiluddi:serviceKey&gt;52946BB0-BC28-11D5-A432-0004AC49CC1E&lt;/wsiluddi:serviceKey&gt;
        &lt;wsiluddi:discoveryURL useType=&quot;businessEntity&quot;&gt;
          http://www.getquotecom/uddi?businessKey=3BF0ACC0-BC28-11D5-A432-0004AC49CC1E
        &lt;/wsiluddi:discoveryURL&gt;
      &lt;/wsiluddi:serviceDescription&gt;
    &lt;/description&gt;
  &lt;/service&gt;
&lt;/inspection&gt;</code></PRE>
      </td>
    </tr>
  
</table>
<H3><b>Linking to WS-Inspection documents</b></H3>
<P>One important feature of the WS-Inspection specification, is the ability to link a WS-Inspection document to one or more different WS-Inspection documents. This feature can be used to manage service description references by grouping them into different documents. Using the <CODE>&lt;link&gt;</CODE> element, a hierarchy of WS-Inspection documents can be built using these
individual documents. For example, separate WS-Inspection documents can
be created for different categories of services, and one primary WS-Inspection
document can link all of them together.</P>
<p><a name="find"></a><span class="atitle3">Finding WS-Inspection documents</span>




<P>The second primary function provided by the WS-Inspection specification
is to define the locations where you can access WS-Inspection documents.There
are two conventions which were created to make the location and retrieval
of WS-Inspection documents easy:</P>
<UL>
  <LI>Fixed name WS-Inspection documents
  <LI>Linked WS-Inspection documents
</UL>
<P>The fixed name for WS-Inspection documents is <I>inspection.wsil</I>. A document with this name can be placed at common entry points for a
Web site. For example, if the common entry point is http://example.com
or http://example.com/services, then the location of the WS-Inspection
document would be http://example.com/inspection.wsil or http://example.com/services/inspection.wsil,
respectively.</P>
<P>References to WS-Inspection documents may also appear within different
content documents, such as HTML pages. When putting entries in an HTML
page, a META tag may be used to convey the location of a WS-Inspection
document. Listing 4 contains an example of an HTML page that contains the
same WS-Inspection document references listed above. The HTML page that
is used should be widely used. This could be the root document for a web
server, or it could be a Web page, in a human readable format, that describes
one or more Web services that appear in the WS-Inspection document.</P>
<p><a name="listing1"></a><b>Listing 4: WS-Inspection Document References in an HTML Page</b>


<table border="1" cellpadding="5"  width="100%" cellspacing="0" bgcolor="#cccccc">
  
    <tr>
      <td>
      <pre><code>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.01 Transitional//EN&quot;&gt;
&lt;html&gt;
  &lt;head&gt;
    &lt;META name=&quot;serviceInspection&quot; content=&quot;http://example.com/inspection.wsil&quot;
    &lt;META name=&quot;serviceInspection&quot; content=&quot;http://example.com/services/inspection.wsil&quot;
  &lt;head&gt;
...
&lt;html&gt;</code></pre>
      </td>
    </tr>
  
</table>
<H2>Web Services Toolkit Support for WS-Inspection Documents</H2>
<P>The Web Services Toolkit  (see <A href="#resources">Resources</A>) includes integrated support for the Web Services Inspection Language.
This support includes a demonstration of how to use WS-Inspection documents,
and a Java API that allows you to parse existing WS-Inspection documents
and programmatically create new documents.</P>
<P>Most of the toolkit demos provide an option to use WS-Inspection technology
or UDDI as the service discovery mechanism. If the WS-Inspection option
is used, the demos request a WS-Inspection document from the Web server
configured using the toolkit configuration utility. This request is submitted
using the fixed name for the WS-Inspection document. This document name
is set up to invoke a Java servlet. This servlet will dynamically create
the WS-Inspection document, by searching for WSDL service description documents
with the toolkit directory structure.</P>
<P>Figure 3 contains an overview of this process:</P>
<OL>
  <LI>The WS-Inspection document proxy is used to request the contents of the
  WS-Inspection document using a fixed name.
  <LI>The URL that is used to retrieve the WS-Inspection document maps to a servlet.
  This servlet will search through the local filesystem for all WSDL service
  descriptions. A reference to each service description will be put into
  the WS-Inspection document.
  <LI>The dynamically generated WS-Inspection document is returned to the client.
</OL>
<p><a name="figure3"></a><b>Figure 3: WS-Inspection Document Support in the Web Services Toolkit</b>
<P><IMG src="images/WSInspectionServlet.jpg" width="640" height="450" border="2"></P>
<P>Listing 5 contains a portion of the WS-Inspection document that is returned
by the WS-Inspection servlet. The service name is set from the name attribute
on the definition element within the WSDL document. The entry that appears
in this listing is for the stock quote demo.</P>
<p><a name="listing1"></a><b>Listing 5: WS-Inspection Document Generated by the Web Services Toolkit</b>

<table border="1" cellpadding="5"  width="100%" cellspacing="0" bgcolor="#cccccc"><tr><td><pre><code>&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;inspection xmlns=&quot;http://schemas.xmlsoap.org/ws/2001/10/inspection/&quot;&gt;
  ...
  &lt;service&gt;
    &lt;name xml:lang=&quot;en-US&quot;&gt;StockQuoteService&lt;/name&gt;
    &lt;description referencedNamespace=&quot;http://schemas.xmlsoap.org/wsdl/&quot; 
                 location=&quot;http://localhost:8080/webservices/wsdl/stockquote/sqs.wsdl&quot;&gt;
      &lt;wsilwsdl:reference endpointPresent=&quot;true&quot;&gt;
        &lt;wsilwsdl:implementedBinding 
            xmlns:interface=&quot;http://www.getquote.com/StockQuoteService-interface&quot;&gt;
          interface:StockQuoteServiceBinding
        &lt;/wsilwsdl:implementedBinding&gt;
      &lt;/wsilwsdl:reference&gt;
    &lt;/description&gt;
  &lt;/service&gt;
  ...
&lt;/inspection&gt;</code></pre></td></tr></table>
<H3>Running the WSTK Demos</H3>
<P>Starting with the Web Services Toolkit V2.4.1, the toolkit demos can be
run using either the Web Services Inspection Language or UDDI as the service
discovery mechanism. You can select the service discovery method by using
the new <B>-mode</B> option. This option accepts two parameters: <B>uddi</B> or <B>wsil</B> (which is the default). The following command is an example of how you
would run the Web Service Example demo using the WS-Inspection as the service
discovery process:</P>
<BLOCKQUOTE>
<P><CODE>webservicedemo <B>-mode wsil</B> -role all 
-demo both</CODE></P>
</BLOCKQUOTE>
<H3><span class="atitle3">Using the Web Services Inspection Language for Java API</span></H3>
<P>The Web Services Inspection Language for Java API (WSIL4J) provides a Java
interface, which can be used to parse existing WS-Inspection documents
or programmatically create new WS-Inspection documents. Most of the WSIL4J
classes represent the elements that can appear in a WS-Inspection document.
For example, the <CODE>&lt;inspection&gt;</CODE> element is represented by the <I>Inspection</I> class, and the <CODE>&lt;service&gt;</CODE> element is represented by the <I>Service</I> class. There are also utility classes that make it easy to read and parse a WS-Inspection document, as well as write out the contents of the WSIL4J objects as an XML document. </P>
<P>Listing 6 contains an example of how to use this API. In this sample code,
a WS-Inspection document is read and the service elements are searched
for references to WSDL service descriptions. When a WSDL service description
is found, its location is saved in a list which is displayed on the console.
You can view and download the complete <A href="../samples/WSInspectionExample.java">WSInspectionExample</A> application. If you have installed the toolkit, you can use the <I>wstkenv</I> command to set up the classpath that is needed to compile and run these
examples. This command is located in the WSTK bin directory. The purpose
of this command is to define a set of environment variables. One of the
environment variables is named WSTK_CP. This environment variable contains
the classpath that is required to compile and run the examples.</P>
<p><A name="listing5"></A><b>Listing 6: Example WS-Inspection Application</b>

<table border="1" cellpadding="5"  width="100%" cellspacing="0" bgcolor="#cccccc">
<tr><td><pre>...
<code>// Create a new instance of a WS-Inspection document
WSILDocument document = WSILDocument.newInstance();

// Read and parse the WS-Inspection document
document.read(wsinspectionURL);

// Get the inspection element from the document
Inspection inspection = document.getInspection();

// Obtain a list of all service elements
Service[] services = inspection.getServices();

// Display purpose of list
System.out.println(&quot;Display list of WSDL service description references...&quot;);
                  
// Process each service element to find all WSDL document references
for (int serviceCount = 0; serviceCount &lt; services.length; serviceCount++)
{
  // Get the next set of description elements
  descriptions = services[serviceCount].getDescriptions();

  // Process each description to find the WSDL references
  for (int descCount = 0; descCount &lt; descriptions.length; descCount++)
  {
    // If the referenced namespace is for WSDL, then save the location reference
    if (descriptions[descCount].getReferencedNamespace().equals(WSDLConstants.NS_URI_WSDL))
    {
      // Add WSDL location to the list
      wsdlList.add(descriptions[descCount].getLocation());
    }
  }

  // If this service has WSDL service descriptions, then display the list
  if (wsdlList.size() &gt; 0)
  {
    // Get service name
    serviceName = (services[serviceCount].getServiceNames().length == 0) ? 
    &quot;[no service name]&quot; : services[serviceCount].getServiceNames()[0].getText();
                                                
    // Display service name
    System.out.println(&quot;  Service: &quot; + serviceName);

    // Display list
    Iterator iterator = wsdlList.iterator();
    for (int count = 1; iterator.hasNext(); count++)
    {
      System.out.println(&quot;    [&quot; + count + &quot;] &quot; + ((String) iterator.next()));
    }
  }

  // Clear the list
  wsdlList.clear();
}
...</code></pre></td></tr>
</table>
<H3><span class="atitle3">Using the WS-Inspection proxy</span></H3>
<P>The WSIL4J API also provides a <I>WSILProxy</I> class which can be used to easily access certain types of information within a WS-Inspection document. The proxy interface will read the WS-Inspection document, then allow you to directly access the WSDL documents for UDDI business services that you need. Listing 7 contains a portion of an application that shows how to use the WS-Inspection proxy to get a list of WSDL documents for a given service name. You can also view and download the complete <A href="../samples/WSInspectionProxyExample.java">WSInspectionProxyExample</A> application. </P>
<p><A name="listing5"></A><b>Listing 7: Example WS-Inspection Proxy Usage</b>

<table border="1" cellpadding="5"  width="100%" cellspacing="0" bgcolor="#cccccc">
<tr><td><pre>...<code>
// Create a new instance of a WS-Inspection document proxy
WSILProxy proxy = new WSILProxy(wsinspectionURL);

// Get all of the WSDL documents using the input service name
WSDLDocument[] wsdlDocuments = proxy.getWSDLDocumentByServiceName(serviceName);

// Display purpose of list
System.out.println(&quot;Display contents of WSDL service description documents for service name [&quot; +
                   serviceName + &quot;]...&quot;);
                  
// Process each WSDL document reference
for (int wsdlCount = 0; wsdlCount &lt; wsdlDocuments.length; wsdlCount++)
{
  // Display contents of the document
  System.out.println(&quot;[&quot; + wsdlCount + &quot;] \n&quot; + wsdlDocuments[wsdlCount].serializeToXML());
}
...</code></pre></td></tr>
</table>
<H2>Summary</H2>
<p>In this article, we described the Web Services Inspection Language and
how it provides a simple, distributed service discovery method for any
type of Web service description document. We also described how the WS-Inspection
technology is complementary to existing service discovery methods, such
as UDDI, because it defines a process for inspecting a Web site for service
descriptions.
<P>In the future, we will start to see additional uses for the WS-Inspection
technology. For example, we may see the implementation of WS-Inspection
interfaces for Web service description repositories. We may also see this
technology used to develop Web service crawlers. These service crawlers
would search through Web sites for WS-Inspection documents and then aggregate
the service description references from multiple sites. With this and other
applications of this technology, the Web Services Inspection Language will
become an effective service discovery technology.</P>
<H2><a name="resources"><span class="atitle2">Resources</span></a></H2>
<UL>
  <li>The <A href="http://www.ibm.com/developerworks/...">Web Services Inspection Language (WS-Inspection) specification</A> is located on the developerWorks site.</li>
<li>Download the <A href="http://www.alphaworks.ibm.com/tech/webservicestoolkit">IBM Web Services Toolkit</A> which contains an implementation of the WS-Inspection specification.</li>
  <li><A href="http://www.ibm.com/developerworks/...">The WS-Inspection and UDDI Relationship</A> paper contains information on how WS-Inspection relates to UDDI.</li>
  <li>The <A href="http://www.uddi.org/specification.html">UDDI specifications</A> can be accessed on the uddi.org web site.</li>
  <li>The <A href="http://www.w3c.org/TR/wsdl">WSDL V1.1 specification</A> contains information on the structure of WSDL documents.</li>
</UL>
</BODY>
</html>